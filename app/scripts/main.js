(function(document, window) {
  'use strict';

  var $btnContact = $('.open-contact');
  var $contactSection = $('#shinobi-contact');
  var $closeBtn = $('.close-action');

  $btnContact.on('click', function(e){
    e.preventDefault();
    console.log('abrir');

    $contactSection.addClass('visible animated bounceInLeft');

    $closeBtn.on('click', function(e){
      e.preventDefault();
      $contactSection.removeClass('visible animated bounceInLeft');
    });
  });
})(document, window);
